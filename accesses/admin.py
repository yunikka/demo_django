from django.contrib import admin

from .models import Accesses

class AccessesAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('name', 'exchange', 'created', 'upload',)
    search_fields = ('name', 'exchange', 'created', 'upload',)
    list_filter = ('name', 'exchange', 'created',)

admin.site.register(Accesses, AccessesAdmin)
