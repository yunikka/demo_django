from django.db import models

class Accesses(models.Model):
    name = models.TextField(
        max_length=200,
        default=None,
        null=False,
        blank=False,
        verbose_name=("Name"),
        help_text=("No more than 200 characters"),
    )
    exchange = models.TextField(
        max_length=200,
        default=None,
        null=False,
        blank=False,
        verbose_name=("Exchange"),
        help_text=("No more than 200 characters"),
    )
    created = models.DateTimeField(
        default=None,
        null=False,
        blank=False,
        verbose_name=("Created"),
        help_text=("Datetime when was created"),
    )
    upload = models.FileField(
        verbose_name=("Upload"),
        upload_to='uploads/',
    )

    class Meta:
        verbose_name = ("Name")
        verbose_name_plural = ("Names")
        ordering = ['created']

    def __str__(self):
        return self.name
